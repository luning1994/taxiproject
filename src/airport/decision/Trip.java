package airport.decision;

import com.vividsolutions.jts.geom.Coordinate;

/**
 * Changes 11/10/2015:
 * - Parse tripId, start coordinate
 * - Add end time, end coordinate
 * @author Alex
 *
 */
public class Trip{
	private String tripId;
	private double startLong;
	private double startLat;
	private long startTime;
	private int fare;
	private int driverId;
	private int vehicleId;
	private double endLng;
	private double endLat;
	private long endTime;
	private double distance;
	private int startDow;
	private int startDay;
	private int startHour;
	private String startQueueName;
	private String endQueueName;
	
	public static String[] headerArray(){
		return new String[]{"vehicle-id","trip-id","driver-id","start-time","start-long","start-lat"
				,"end-time","end-long","end-lat","distance","fare","start-dow","start-day","start-hour","terminal"};
	}
	public static String[] prePostHeaderArray(){
		return new String[]{"trip-id","driver-id","start-time","start-long","start-lat"
				,"end-time","end-long","end-lat","distance","fare","terminal","idle-distance","idle-time"};
	}
	
	public Trip(int vehicleId,String tripId, long startTime, 
			double startLong, double startLat, int fare, 
			long endTime, double endLng, double endLat, int driverId, 
			double distance, int startDow, int startDay, int startHour){
		this.tripId = tripId;
		this.startLong = startLong;
		this.startLat = startLat;
		this.fare = fare;
		this.driverId = driverId;
		this.vehicleId = vehicleId;
		this.startTime = startTime;
		this.endLat = endLat;
		this.endLng = endLng;
		this.endTime = endTime;
		this.distance = distance;
		this.startDow = startDow;
		this.startDay = startDay;
		this.startHour = startHour;
	}
	
	public int getVehicleId(){
		return vehicleId;
	}
	public double getStartLong(){
		return startLong;
	}
	public double getStartLat(){
		return startLat;
	}
	public double getEndLong(){
		return endLng;
	}
	public double getEndLat(){
		return endLat;
	}
	public long getEndTime(){
		return endTime;
	}
	public long getStartTime(){
		return startTime;
	}
	public void setStartQueueName(String queueName){
		this.startQueueName = queueName;
	}
	public String getStartQueueName(){
		return startQueueName;
	}
	public void setEndQueueName(String queueName){
		this.endQueueName = queueName;
	}
	public String getEndQueueName(){
		return endQueueName;
	}
	
	public String toString(){
		return vehicleId+","+tripId+","+driverId+","+startTime+","+startLong
				+","+startLat+","+endTime+","+endLng+","+endLat+","+distance
				+","+fare+","+startDow+","+startDay+","+startHour+","+endQueueName;
	}
	/*
	 * return new String[]{"trip-id","driver-id","start-time","start-long","start-lat"
				,"end-time","end-long","end-lat","distance","fare","idle-distance","idle-time"};
	 */
	public String toString(Trip t, boolean isPrev){
		double idleDistance;
		int timeDiff;
		/*get idle distance and idle time between trips*/
		if(isPrev){
			Coordinate prevCoor = new Coordinate(t.getEndLong(),t.getEndLat());
			Coordinate thisCoord = new Coordinate(startLong, startLat);
			idleDistance = thisCoord.distance(prevCoor)*100;//in km
			timeDiff = (int)(startTime-t.getEndTime());
		}else{
			Coordinate nextCoor = new Coordinate(t.getStartLong(),t.getStartLat());
			Coordinate thisCoord = new Coordinate(endLng, endLat);
			idleDistance = thisCoord.distance(nextCoor)*100;//in km
			timeDiff = (int)(t.getStartTime()- endTime);
		}
		return tripId+","+driverId+","+startTime+","+startLong
				+","+startLat+","+endTime+","+endLng+","+endLat+","+distance
				+","+fare+","+startQueueName+","+String.format( "%.2f", idleDistance )+","+timeDiff;
	}
}
