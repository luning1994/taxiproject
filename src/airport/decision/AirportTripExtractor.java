package airport.decision;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import airport.util.LocationChecker;

/**
 * This module prints out trips that end in the airport and the following trip of that vehicle
 * 
 * @author Alex
 *
 */
public class AirportTripExtractor {
	
	private String fileInput;
	private String extFileInput;
	private String fileOutputDir;
	/*
	 * stores the trip triplets. The sequence is: [<previous trip><airport
	 * trip><next trip>]
	 */
	private List<Trip[]> outputArray 	 = new ArrayList<Trip[]>();
	private LocationChecker checker;
	private Map<Integer, Trip> tripMap = new HashMap<Integer, Trip>(); // vid:tripObj
	private int numOfIncompleteTriplets;
	private int[] currentMonth = null;

	public AirportTripExtractor(String fileInput, String extFileInput, String fileOutputDir) {
		this.fileInput = fileInput;
		this.extFileInput = extFileInput;
		this.fileOutputDir = fileOutputDir;
		this.checker = new LocationChecker();
	}

	public boolean readFile(){
		long numRead = 0;
		//long numAdded = 0;
		long startTime = System.currentTimeMillis();
		boolean success = true;
		try {
			
			//buffered reader for trip files
			BufferedReader br = new BufferedReader(new FileReader(fileInput));
			//buffered reader for trip extension files
			BufferedReader exbr = new BufferedReader(new FileReader(extFileInput));
			//read headers
//			br.readLine();
//			exbr.readLine();
			String line = br.readLine();
			String extLine = exbr.readLine();

			while(line!=null && extLine!=null){
				numRead ++;//counter
				String records[] = (line+","+extLine).split(",");
			
				try{
					Trip t = parseLine(records);//throws exception upon reading new header
					boolean endsInAirport = t.getEndQueueName()!=null;
					//boolean startsInAirport = t.getStartQueueName()!=null;
				    int vid = t.getVehicleId();
				    /*processing algo:*/
					if(tripMap.containsKey(vid) ){
						if(t.getStartTime()-tripMap.get(vid).getEndTime()<60*60*6){
							outputArray.add(new Trip[]{tripMap.get(vid),t});
						}
						tripMap.remove(vid);
					}
					if(endsInAirport){
						tripMap.put(vid, t);
					}
					line = br.readLine();
					extLine = exbr.readLine();
				}catch(NumberFormatException numExc){
					System.out.println("new header detected");
					line = br.readLine();
					extLine = exbr.readLine();
					if(line!=null && extLine!=null){
						String nextRecords[] = (line+","+extLine).split(",");
						Trip t = parseLine(nextRecords);
						startNewMonth(t);
					}
				}
			}
			startNewMonth(null);
			br.close();
			exbr.close();
			//for the remaining triplets, they should be incomplete and be removed to the output list
			tripMap.clear();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			success= false;
		} catch (IOException e) {
			e.printStackTrace();
			success= false;
		}
		long timeElapse = System.currentTimeMillis() - startTime;
		System.out.format(" %s | NumOfLine read %s | NumOfIncompleteTriplets %s | Time Elapse %s%n",fileInput,numRead, numOfIncompleteTriplets, timeElapse);
		return success;
	}
	
	private void startNewMonth(Trip t){
		if(t!=null){
			Calendar c = Calendar.getInstance(TimeZone.getTimeZone("Singapore"));
			if(t!=null) c.setTimeInMillis(t.getStartTime()*1000);
			/*generate file name for last month output*/
			if(currentMonth!=null){
				java.text.DecimalFormat nft = new java.text.DecimalFormat("00.");
				nft.setDecimalSeparatorAlwaysShown(false);
				String label = nft.format(currentMonth[0]%100)+nft.format(currentMonth[1]);
				/*write out the last month's data*/
				writeFile(outputArray,fileOutputDir+"trips-airport-"+label+"-decision.csv" );

			}
			currentMonth = new int[]{c.get(Calendar.YEAR),c.get(Calendar.MONTH)+1 };
			outputArray.clear();
		}
		
	}

	/**
	 * The output are produced in sequence of next trip found, so we need to
	 * sort the output list before printing out
	 */
	/*
	private void sortList(List<Trip[]> tripList) {
		long startTime = System.currentTimeMillis();
		Collections.sort(tripList, new Comparator<Trip[]>() {
			public int compare(Trip[] s1, Trip[] s2) {
				return (int) (s1[1].getStartTime() - s2[1].getStartTime());
			}
		});
		long timeElapse = System.currentTimeMillis() - startTime;
		System.out.format("Sorting finished. Time Elapse %s%n", timeElapse);

	}
	*/

	private Trip parseLine(String[] line) throws NumberFormatException {
		//"vehicle-id","trip-id","driver-id","start-time","start-long","start-lat"
		//,"end-time","end-long","end-lat","distance","fare","start-dow","start-day","start-hour","terminal"
		Trip trip = new Trip(Integer.parseInt(line[8])// int vehicleId
								, line[0]// String tripId
								, Long.parseLong(line[2])// long startTime
								, Double.parseDouble(line[4])// double startLong
								, Double.parseDouble(line[5])// double startLat
								, Integer.parseInt(line[10])// int fare
								, Long.parseLong(line[3])// long endTime
								, Double.parseDouble(line[6])// double endLng
								, Double.parseDouble(line[7])// double endLat
								, Integer.parseInt(line[22])// int driverId
								, Double.parseDouble(line[9])// double distance
								, Integer.parseInt(line[12])// int startDow
								, Integer.parseInt(line[13])// int startDay
								, Integer.parseInt(line[14])// int startHour
						);
		trip.setEndQueueName(checker.inQueueName(trip.getEndLat(), trip.getEndLong()));
		trip.setStartQueueName(checker.inQueueName(trip.getStartLat(), trip.getStartLong()));
		return trip;
	}

	/**
	 * Writes sorted array to file
	 * @param arrayToWrite
	 * @param fileName
	 * @return
	 */
	public boolean writeFile(List<Trip[]> arrayToWrite, String fileName) {
		long numWritten = arrayToWrite.size();
		long startTime = System.currentTimeMillis();
		boolean success = true;
		try {
			FileWriter fw = new FileWriter(fileName);
			BufferedWriter bw = new BufferedWriter(fw);
			//transfer header to string
			String header = "";
			for (String hd : Trip.headerArray()) {
				header += hd + ",";
			}
			//for pre trip attributes, prefix with "pre"
			for (String hd : Trip.prePostHeaderArray()) {
				header += "next-" + hd + ",";
			}
			bw.write(trimComma(header) + "\n");
			for (int i = 0; i < arrayToWrite.size(); i++) {
				Trip[] trips = arrayToWrite.get(i);
				//if the triplet is complete
				if(trips[0]!=null && trips[1]!=null){
					String toWrite = "";
					toWrite += trips[0].toString() + ",";
					toWrite += trips[1].toString(trips[0], true) + ",";
					String outputLine = trimComma(toWrite);
					if (outputLine != null) {
						bw.write(outputLine + "\n");
					}
				}
			}
			bw.flush();
			bw.close();
		} catch (IOException e) {
			success = false;
			e.printStackTrace();
		}
		long timeElapse = System.currentTimeMillis() - startTime;
		System.out.format("NumOfLine written %s Elase Time %s %n", numWritten, timeElapse);
		return success;
	}

	private String trimComma(String str) {
		return str.substring(0, str.length() - 1);
	}

}
