package airport.preparelog;

import java.util.concurrent.LinkedBlockingQueue;

public class PreprocessLog{

	
	public PreprocessLog(){}
	
	public static void main(String[] args){
		if(args.length==2){
			boolean isMonthly = false;
			for(String arg:args){
				if(arg.indexOf("yy")!=-1 && arg.indexOf("mm")!=-1){
					isMonthly = true;break;
				}
			}
			if(isMonthly){
				String[] years = {"09","10","11","12"};
				String[] months = {"01","02","03","04","05","06","07","08","09","10","11","12"};
				for(int i=0;i<years.length;i++){
					String year = years[i];
					for(int j=0;j<months.length;j++){
						String month = months[j];
						String[] paths = new String[]{args[0],args[1]};
						for(int k=0;k<paths.length;k++){
							paths[k] = paths[k].replaceAll("yy", year);
							paths[k] = paths[k].replaceAll("mm", month);
						}
						start(paths[0], paths[1]);
						
					}
				}
			}else{
				start(args[0], args[1]);
			}
			
		}else if(args.length==1){
			System.out.println("<trips file name> <ext file name> <output file name>");
		}
		
		System.out.println("\n****Finish****");
	}
	/**
	 * Initialize all threads
	 * @param fileInput
	 * @param fileOutput
	 */
	public static void start(String fileInput,String fileOutput){
		LinkedBlockingQueue<String> bufQue  = new LinkedBlockingQueue<String>();
		new LogPreprocessReader(fileInput, bufQue);
		(new Thread(new LogPreprocessWriter(fileOutput, bufQue))).start();

	}
}
