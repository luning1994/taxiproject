package airport.preparelog;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
/**
 * Removes logs from the bufQue and writes to output file
 * @author Alex
 *
 */
public class LogPreprocessWriter implements Runnable {
	private String fileOutput;
	private LinkedBlockingQueue<String> bufQue;
	
	public LogPreprocessWriter(String fileOutput, LinkedBlockingQueue<String> bufQue){
		this.fileOutput = fileOutput;
		this.bufQue = bufQue;
	}
	public void run (){
		long numWritten = 0;
		BufferedWriter bw = null; 
		try {
			FileWriter fw = new FileWriter(fileOutput);
			bw = new BufferedWriter(fw);
			String record;
			while((record=bufQue.take())!="end"){
				bw.write(record+"\n");
				numWritten++;
			}
			bw.flush();
			bw.close();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Number of lines written "+numWritten);
	}

}
