package airport.preparelog;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

import airport.util.LocationChecker;

/**
 * Log reader for the log pre-processing module. 
 * The reader loads logs within the outerqueue and place them in the bufQue for writer
 * @author Alex
 *
 */
public class LogPreprocessReader {
	//String header;
	LocationChecker checker = new LocationChecker();
	
	private LinkedBlockingQueue<String> bufQue;
	public LogPreprocessReader(String fileInput, LinkedBlockingQueue<String> bufQue){
		this.bufQue = bufQue;
		readFile(fileInput);
	}
	
	public void readFile(String fileInput){
		BufferedReader br;
		long countLine = 0;
		long countAdded = 0;
		long startTime = System.currentTimeMillis();
		try {
			br = new BufferedReader(new FileReader(fileInput));
			System.out.println("File "+fileInput+" loaded");
			String header = br.readLine();
			bufQue.put(header);
			String line = br.readLine();
			while(line!=null){
				try{
					countLine ++;
					//print to console on every 10 million lines reached
					if(countLine%10000000==0){
						System.out.println(countLine + " lines read");
					}
					String[] rcd = line.split(",");
					double lng = Double.parseDouble(rcd[3]);
					double lat = Double.parseDouble(rcd[4]);
					if(checker.isInOuterQueue(lat, lng)){
						countAdded ++;
						bufQue.put(line);
					}
				}catch(NumberFormatException nume){
					nume.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				line = br.readLine();
			}
			bufQue.put("end");
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} 
		long timeElapse = System.currentTimeMillis()-startTime;
		System.out.format("NumOfLine read %s  | NumbOfLine added %s | Elapse %s%n", countLine, countAdded,timeElapse);
		
	}

}
