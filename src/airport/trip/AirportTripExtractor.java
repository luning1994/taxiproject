package airport.trip;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import airport.util.LocationChecker;

/**
 * This module extracts airport trips and attaches to the trip its previous and
 * next trip to form a triplet. Triplet will then be printed out with derived 
 * idle time and distance.
 * 
 * @author Alex
 *
 */
public class AirportTripExtractor {
	/*
	 * whether the system should print out airport trips without prev or next
	 * trip
	 */
	private static final boolean STRICT = false;
	private static List<Long[]> timeKeeper = new ArrayList<Long[]>(); 

	private String fileInput;
	private String extFileInput;
	private String fileOutputDir;
	/*
	 * stores the trip triplets. The sequence is: [<previous trip><airport
	 * trip><next trip>]
	 */
	private List<Trip[]> outputArray 	 = new ArrayList<Trip[]>();
	private List<Trip[]> lastOutputArray = new ArrayList<Trip[]>();
	private List<Trip[]> errorArray = new ArrayList<Trip[]>();
	private LocationChecker checker;
	private Map<Integer, Trip[]> tripMap = new HashMap<Integer, Trip[]>(); // vid:tripObj
	private int numOfIncompleteTriplets;

	public AirportTripExtractor(String fileInput, String extFileInput, String fileOutputDir) {
		this.fileInput = fileInput;
		this.extFileInput = extFileInput;
		this.fileOutputDir = fileOutputDir;
		this.checker = new LocationChecker();
	}

	public boolean readFile(){
		long numRead = 0;
		//long numAdded = 0;
		long startTime = System.currentTimeMillis();
		boolean success = true;
		try {
			
			//buffered reader for trip files
			BufferedReader br = new BufferedReader(new FileReader(fileInput));
			//buffered reader for trip extension files
			BufferedReader exbr = new BufferedReader(new FileReader(extFileInput));
			//read headers
//			br.readLine();
//			exbr.readLine();
			String line = br.readLine();
			String extLine = exbr.readLine();

			while(line!=null && extLine!=null){
				numRead ++;//counter
				String records[] = (line+","+extLine).split(",");
				try{
					Trip t = parseLine(records);//throws exception upon reading new header
					mapTrip(t);
					line = br.readLine();
					extLine = exbr.readLine();
				}catch(NumberFormatException numExc){
					line = br.readLine();
					extLine = exbr.readLine();
					if(line!=null && extLine!=null){
						String nextRecords[] = (line+","+extLine).split(",");
						Trip t = parseLine(nextRecords);
						startNewMonth(t);
					}
				}
			}
			startNewMonth(null);
			br.close();
			exbr.close();
			//for the remaining triplets, they should be incomplete and be removed to the output list
			if(!STRICT){
				for (Map.Entry<Integer, Trip[]> entry : tripMap.entrySet()) {
					if(entry.getValue()[1]!=null){
						processRemoval(entry.getKey(),false);
					}
				}
			}
			tripMap.clear();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			success= false;
		} catch (IOException e) {
			e.printStackTrace();
			success= false;
		}
		long timeElapse = System.currentTimeMillis() - startTime;
		System.out.format(" NumOfLine read %s | NumOfIncompleteTriplets %s | Time Elapse %s%n",numRead, numOfIncompleteTriplets, timeElapse);
		return success;
	}
	private void startNewMonth(Trip t){
		if(t!=null){
			
			/*sort last month's list*/
			sortList(lastOutputArray);
			/*generate file name for last month output*/
			if(timeKeeper.size()>=2){//avoid IndexOutOfBoundException
				Long[] lastMonth = timeKeeper.get(timeKeeper.size()-1-1);/*get the last but one time record*/
				java.text.DecimalFormat nft = new java.text.DecimalFormat("00.");
				nft.setDecimalSeparatorAlwaysShown(false);
				String label = nft.format(lastMonth[0]%100)+nft.format(lastMonth[1]);
				/*write out the last month's data*/
				
				writeFile(lastOutputArray,fileOutputDir+"trips-airport-"+label+".csv" );
				writeFile(errorArray, fileOutputDir+"trips-airport-"+label+"-incomplete.csv");
				errorArray.clear();
			}
			/*transfer outputArray to lastOutputArray*/
			lastOutputArray.clear();
			lastOutputArray.addAll(outputArray);
			/*clears this month's list for new data*/
			outputArray.clear();
			
			Calendar c = Calendar.getInstance(TimeZone.getTimeZone("Singapore"));
			c.setTimeInMillis(t.getStartTime()*1000);
			timeKeeper.add(new Long[]{
						(long) c.get(Calendar.YEAR),
						(long) c.get(Calendar.MONTH)+1, 
						t.getStartTime() 
					});
		}else{/*process finalizing*/
			sortList(lastOutputArray);
			sortList(outputArray);
			/*generate file name for last month output*/
			if(timeKeeper.size()>=2){
				Long[] lastMonth = timeKeeper.get(timeKeeper.size()-2);/*last but one time record*/
				java.text.DecimalFormat nft = new java.text.DecimalFormat("00.");
				nft.setDecimalSeparatorAlwaysShown(false);
				String lastMonthTag = nft.format(lastMonth[0]%100)+nft.format(lastMonth[1]);
				/*write out the last month's data*/
				writeFile(lastOutputArray,fileOutputDir+"trips-airport-"+lastMonthTag+".csv");
				writeFile(errorArray, fileOutputDir+"trips-airport-"+lastMonthTag+"-incomplete.csv");
				//errorArray.clear();
				Long[] thisMonth = timeKeeper.get(timeKeeper.size()-1);/*last time record*/
				String thisMonthTag = nft.format(thisMonth[0]%100)+nft.format(thisMonth[1]);
				/*write out the last month's data*/
				writeFile(outputArray,fileOutputDir+"trips-airport-"+thisMonthTag+".csv");
			}
			/*dump outputArray to lastOutputArray*/
			lastOutputArray.clear();
			lastOutputArray.addAll(outputArray);
		}
		
	}

	/**
	 * The output are produced in sequence of next trip found, so we need to
	 * sort the output list before printing out. The records in the input tripList have all airport trip set
	 */
	private void sortList(List<Trip[]> tripList) {
		long startTime = System.currentTimeMillis();
		Collections.sort(tripList, new Comparator<Trip[]>() {
			public int compare(Trip[] s1, Trip[] s2) {
				return (int) (s1[1].getStartTime() - s2[1].getStartTime());
			}
		});
		long timeElapse = System.currentTimeMillis() - startTime;
		System.out.format("Sorting finished. Time Elapse for sorting %s%n", timeElapse);

	}

	/**
	 * Maps the trip based on whether it is airport trip and the triplet status
	 * 
	 * @param t
	 */
	private void mapTrip(Trip t) {
		int vid = t.getVehicleId();
		if (t.getStartQueueName() == null) {
			if (!tripMap.containsKey(vid)) {
				tripMap.put(vid, new Trip[] { t, null, null });
			} else if (tripMap.get(vid)[1] == null) {// this triplet is does not
														// contain airport trip
				tripMap.get(vid)[0] = t;
			} else {
				tripMap.get(vid)[2] = t;
				processRemoval(vid, true);
				//added on Tue Oct 20
				tripMap.put(vid, new Trip[] { t, null, null });
			}
		} else { // process airport trip
			if (!tripMap.containsKey(vid)) {
				/* if the first trip for this vehicle is airport trip, 
				 * treat this trip as normal trip
				 */
				tripMap.put(vid, new Trip[] { t, null, null });
			} else if (tripMap.get(vid)[1] == null) {// this triplet is does not
														// contain airport trip
				tripMap.get(vid)[1] = t;
			} else { // when the previous trip is also airport trip,
						// complete the current triplet and initiate a new one
				tripMap.get(vid)[2] = t;
				Trip lastApTrip = tripMap.get(vid)[1];
				processRemoval(vid, true);
				tripMap.put(vid, new Trip[] { lastApTrip, t, null });
			}
		}
	}
	/**
	 * Called when the triplet is complete.
	 * @param vid
	 * @param removeFromMap set to true unless it's called for error cleaning
	 */
	private void processRemoval(int vid, boolean removeFromMap) {
		Trip[] trip = tripMap.get(vid);
			Long thisMonthStart = timeKeeper.get(timeKeeper.size()-1)[2];
			Long lastMonthStart =0L;
			if(timeKeeper.size()>1){//in case of first month
				lastMonthStart = timeKeeper.get(timeKeeper.size()-2)[2];
			}
			if(trip[1].getStartTime()>=thisMonthStart){
				outputArray.add(trip);
			}else if(trip[1].getStartTime()>=lastMonthStart){
				lastOutputArray.add(trip);
			}
			//for the error loop
			if (removeFromMap) tripMap.remove(vid);
	}

	private Trip parseLine(String[] line) throws NumberFormatException {
		
		Trip trip = new Trip(Integer.parseInt(line[8])// int vehicleId
								, line[0]// String tripId
								, Long.parseLong(line[2])// long startTime
								, Double.parseDouble(line[4])// double startLong
								, Double.parseDouble(line[5])// double startLat
								, Integer.parseInt(line[10])// int fare
								, Long.parseLong(line[3])// long endTime
								, Double.parseDouble(line[6])// double endLng
								, Double.parseDouble(line[7])// double endLat
								, Integer.parseInt(line[22])// int driverId
								, Double.parseDouble(line[9])// double distance
								, Integer.parseInt(line[12])// int startDow
								, Integer.parseInt(line[13])// int startDay
								, Integer.parseInt(line[14])// int startHour
						);
		String startQueueName = checker.inQueueName(trip.getStartLat(), trip.getStartLong());
		String endQueueName = checker.inQueueName(trip.getEndLat(), trip.getEndLong());
		trip.setStartQueueName(startQueueName);
		trip.setEndQueueName(endQueueName);
		
		return trip;
	}

	private String convertTripletToString(Trip[] tripArr) {
		String toWrite = "";
		toWrite += tripArr[1].toString() + ",";
		if (tripArr[0] != null) {
			toWrite += tripArr[0].toString(tripArr[1], false) + ",";
		}
		if (tripArr[2] != null) {
			toWrite += tripArr[2].toString(tripArr[1], true) + ",";
		}
		return trimComma(toWrite);
	}
	/**
	 * Writes sorted array to file
	 * @param arrayToWrite
	 * @param fileName
	 * @return
	 */
	public boolean writeFile(List<Trip[]> arrayToWrite, String fileName) {
		System.out.format("Writing file: %s%n", fileName);
		long numWritten = arrayToWrite.size();
		long startTime = System.currentTimeMillis();
		boolean success = true;
		try {
			FileWriter fw = new FileWriter(fileName);
			BufferedWriter bw = new BufferedWriter(fw);
			//transfer header to string
			String header = "";
			for (String hd : Trip.headerArray()) {
				header += hd + ",";
			}
			//for pre trip attributes, prefix with "pre"
			for (String hd : Trip.prePostHeaderArray()) {
				header += "prev-" + hd + ",";
			}
			//for post trip attributes, prefix with "next"
			for (String hd : Trip.prePostHeaderArray()) {
				header += "next-" + hd + ",";
			}
			if(!arrayToWrite.isEmpty()) bw.write(trimComma(header) + "\n");
			
			for (int i = 0; i < arrayToWrite.size(); i++) {
				Trip[] trips = arrayToWrite.get(i);
				//if the triplet is complete
				if(trips[0]!=null && trips[1]!=null && trips[2]!=null){
					String outputLine = convertTripletToString(arrayToWrite.get(i));
					bw.write(outputLine + "\n");
				}else{
					//add to err array if any part missing
					errorArray.add(trips);
				}
				
			}
			bw.flush();
			bw.close();

		} catch (IOException e) {
			success = false;
			e.printStackTrace();
		}
		long timeElapse = System.currentTimeMillis() - startTime;
		System.out.format("Output file %s | NumOfLine written %s | Elapse Time %s %n", fileName, numWritten, timeElapse);
		return success;
	}

	private String trimComma(String str) {
		return str.substring(0, str.length() - 1);
	}

}
