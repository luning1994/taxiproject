package airport.trip;

import java.io.File;

public class ProcessTrip {

	public static void main(String[] args) {
		
		if (args.length == 3) {
			execute(args[0] , args[1], args[2]);
		} else {
			System.out.println("Please provide following arguments");
			System.out.println("<trips file name> <extension file name> <output file directory>");
		}

		System.out.println("\n****Finish****");
	}

	private static void execute(String inputFullPath, String extFullPath, String outputDir) {
		File outputFile = new File(outputDir);
		if(!outputFile.isDirectory() || !outputFile.canWrite()){
			System.out.println("ERROR: the given outputDir is invalid");
			System.exit(1);
		}
		if(outputDir.substring(outputDir.length()-1)!="/" 
				&&outputDir.substring(outputDir.length()-1)!="\\"){
			outputDir = outputDir+"/";
			
		}
		
		AirportTripExtractor extractor = new AirportTripExtractor(inputFullPath, extFullPath, outputDir);
		extractor.readFile();
	}

}
