package airport.util;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

class Queue {
	private String queueName;
	private Polygon plg;
	
	public Queue(String queueName, Polygon plg){
		this.queueName = queueName;
		this.plg = plg;
	}
	public Queue(){}
	
	public String getName(){
		return queueName;
		
	}
	public boolean containsPoint(double lng, double lat, GeometryFactory factory){
		Point pointToTest = factory.createPoint(new Coordinate(lng, lat));
		return plg.contains(pointToTest);
	}
	
	
}
