package airport.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

/**
 * Class to read .conf files
 * User: tdnguyen
 * Date: Dec 3, 2009
 * Time: 2:18:55 PM
 */
public class ConfigReader {
    //read next line from the file, ignoring lines starting with #
    private static String getNextLine(BufferedReader f) throws Exception {
        String s;
        do {
            s = f.readLine();
            if ((s != null) && s.equals("")) s = "#";
        } while (s != null && s.charAt(0) == '#');
        return s;
    }

    public static HashMap<String, String> readConfig(String configFileName) {
        File file;
        BufferedReader f;
        HashMap<String, String> settings = new HashMap<String, String>();

        try {
            file = new File(configFileName);
            f = new BufferedReader(new FileReader(file));
            while (true) {
                String s = getNextLine(f);
                if (s == null)
                    break;
                String[] ss = s.split("=");
                if (ss.length != 2)
                    continue;
                //put the key and value into the parameters hashtable
                settings.put(ss[0], ss[1]);
            }

            f.close();
        } catch (Exception e) {
            System.out.println("Error reading from " + configFileName);
            e.printStackTrace();
        }

        return settings;
    }

    public static void overrideConfig(HashMap<String, String> settings, String[] args, int start) {
        int i = start;
        while (i < args.length) {
            if (i + 1 < args.length) {
                settings.put(args[i], args[i + 1]);
            }
            i += 2;
        }
    }
}

