package airport.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

public class VehicleCounter {

	public static void main(String[] args) {
		try {
			HashSet<String> map = new HashSet<String>();
			@SuppressWarnings("resource")
			BufferedReader br = 
					new BufferedReader(new FileReader("F:\\trips-1202-normal-airport.csv"));
			br.readLine();
			String line = br.readLine();
			while(line!=null){
				String[] records = line.split(",",2);
				map.add(records[0]);
				line = br.readLine();
			}
			System.out.println(map.size());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
