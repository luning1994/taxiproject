package airport.util;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocationChecker {
	//String queueDefFilePath;
	List<Queue> queueList;
	Queue outerQueue;
	GeometryFactory sharedFactory;
	static String queueDefPath = "./config/airport-polygons.config";
	
	public LocationChecker(String queueDefFilePath){
		//this.queueDefFilePath =queueDefFilePath;
		queueList  = new ArrayList<Queue>();
		//loadQueueDefFile(queueDefFilePath);
	}
	public LocationChecker(){
		queueList  = new ArrayList<Queue>();
		sharedFactory = new GeometryFactory();
		loadQueueDefFile();
	}
	/**
	 * The method regards queue names including keyword "Terminal" as terminal queues
	 * The queue name airport as the outer queue
	 */
	public void loadQueueDefFile(){
		HashMap<String,String> configMap = ConfigReader.readConfig(queueDefPath);
		if(!configMap.isEmpty()){
			for (Map.Entry<String,String> entry : configMap.entrySet()) {
			    String key = entry.getKey();
			    String value = entry.getValue();
			    if(key.indexOf("Terminal")!=-1){
			    	Queue q = new Queue(key,getPolygon(value));
			    	queueList.add(q);
			    }else if(key.equals("Airport")){
			    	outerQueue = new Queue(key, getPolygon(value));
			    }
			}
		}else{
			System.out.println("Error in loading "+ queueDefPath);
		}
	}

/**
 * Returns a Polygon object based on a WKT string
 *
 * @param wktString the WTK string
 * @return Polygon a polygon
 */
	public Polygon getPolygon(String wktString) {
		GeometryFactory factory = new GeometryFactory();
		WKTReader reader = new WKTReader(factory);
		Polygon polygon = null;
		try {
			polygon = (Polygon) reader.read(wktString);
		} catch (ParseException e) {
			e.printStackTrace();
			System.out.println("parsing error");
		}
	
		return polygon;
	}
	/**
	 * returns whether or not the point is in any defined queue
	 * @param lat
	 * @param lng
	 * @return
	 */
	public boolean isInAnyQueue(double lat, double lng){
		boolean inAnyQueue = false;
		for(int i=0;i<queueList.size();i++){
			Queue queue = queueList.get(i);
			if(queue.containsPoint(lng, lat, sharedFactory)){ 
				inAnyQueue = true;
				break;
			}
		}
		return inAnyQueue;
	}
	/**
	 * returns the queue name the point is in, or null if the point is
	 * not in any pre-defined queue
	 * @param lat
	 * @param lng
	 * @return
	 */
	public String inQueueName(double lat, double lng){
		String queueName = null;
		for(int i=0;i<queueList.size();i++){
			Queue queue = queueList.get(i);
			if(queue.containsPoint(lng, lat, sharedFactory)){ 
				queueName = queue.getName();
				break;
			}
		}
		return queueName;
	}
	/**
	 * returns whether or not the point is in the outer queue
	 * @param lat
	 * @param lng
	 * @return
	 */
	public boolean isInOuterQueue(double lat, double lng){
		boolean inOuterQueue =false;
		if(outerQueue.containsPoint(lng, lat, sharedFactory)){
			inOuterQueue = true;
		}
		return inOuterQueue;
	}
}
