package airport.shorttime;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

import airport.util.LocationChecker;


public class ExtractLogs {
	final long WINDOW_SIZE = 20*60;//The threshold the car record data are recycled
	final String OUT_PUT_DIR = "./output/";
	private String inputLogFile;
	private String inputTripFile;
	private LocationChecker checker = new LocationChecker();
	private GeometryFactory factory = new GeometryFactory();
	private List<Trace> traceList = new ArrayList<Trace>();
	private long lastTimestamp = 0;
	
	public ExtractLogs(String inputLogFile,String inputShortQueueFile){
		this.inputLogFile = inputLogFile;
		this.inputTripFile = inputShortQueueFile;
		try {
			loadTrips();
			for(Trace t: traceList){
				if(t.startTime>lastTimestamp){
					lastTimestamp = t.startTime;
				}
			}
			readLogs();
			for(Trace t: traceList){
				writeToFile(t.vid, t.startTime, t.logRecords, t.original);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		if(args.length==3){
			boolean isMonthly = false;
			for(String arg:args){
				if(arg.indexOf("yy")!=-1 && arg.indexOf("mm")!=-1){
					isMonthly = true;break;
				}
			}
			if(isMonthly){
				String[] years = {"09","10","11","12"};
				String[] months = {"01","02","03","04","05","06","07","08","09","10","11","12"};
				for(int i=0;i<years.length;i++){
					String year = years[i];
					for(int j=0;j<months.length;j++){
						String month = months[j];
						String[] paths = new String[]{args[0]+args[1],args[0]+args[2]};
						for(int k=0;k<paths.length;k++){
							paths[k] = paths[k].replaceAll("yy", year);
							paths[k] = paths[k].replaceAll("mm", month);
						}
						new ExtractLogs(paths[0], paths[1]);
					}
				}
			}else{
				new ExtractLogs(args[0]+args[1], args[0]+args[2]);
			}
		}else{
			System.out.println("Please provide following arguments. Use yy and mm to replace 2-digit year and month in the path.");
			System.out.println("<Base Dir> <logs file path from base dir> <trips file path from base dir> <output file path from base dir> <output-error file path from base dir>");
		}

		
		System.out.println("\n****FINISH****");
	}

	
	public List<Log> readLogs(){
		//counters and timers
		long countLine = 0;
		long countAdded = 0;
		long startTime = System.currentTimeMillis();
		
		BufferedReader br;
		List<Log> currLog = new ArrayList<Log>();
		
		try {
			br = new BufferedReader(new FileReader(inputLogFile));
			System.out.println("loading file "+inputLogFile);
			br.readLine();//trim header
			//peek next record
			String line = br.readLine();
			
			outer:
			while(line!=null){
				countLine++;
				String[] rcd = line.split(",",3);
				for(Trace t: traceList){
					if(t.vid==Integer.parseInt(rcd[1])){
						rcd = line.split(",");
						long time = Long.parseLong(rcd[0]);
						int logVid = Integer.parseInt(rcd[1]);
						double lng = Double.parseDouble(rcd[3]);
						double lat = Double.parseDouble(rcd[4]);
						if(time>lastTimestamp){
							break outer;
						}
						Log log = new Log(logVid, time, checker.isInOuterQueue(lat, lng), lat, lng);
						if(log.getTime()>t.startTime-WINDOW_SIZE && log.getTime()<=t.startTime+WINDOW_SIZE){
							t.logRecords.add(log);
						}
					}
				}
				line = br.readLine();
				if(countLine%10000000==0){
					System.out.println(countLine/1000000+" records read");
				}
			}
			br.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
		long timeElapse = System.currentTimeMillis()-startTime;
		System.out.format("NumOfLine read %s  | NumbOfLine added %s | Elapse %s%n", countLine, countAdded,timeElapse);
		return currLog;
	}

	private void writeToFile(int vid, long tripTime, List<Log> list, String original) throws IOException{
		BufferedWriter bw = new BufferedWriter(new FileWriter("/home/ninglu/shorttime/logs/"+vid+"-"+(tripTime%1000000)+".csv"));
		//Log[] logArray = list.toArray(new Log[0]);
		bw.write(original+"\n");
		List<Coordinate> coorList =  new ArrayList<Coordinate>();
		for(Log l:list){
			coorList.add(new Coordinate(l.getLng(),l.getLat()));
		}
		LineString lineString = new LineString (new CoordinateArraySequence(coorList.toArray(new Coordinate[0])), factory);
		bw.write(lineString.toText()+"\n");
		bw.close();
		
		BufferedWriter bw2 = new BufferedWriter(new FileWriter("/home/ninglu/shorttime/logs/"+vid+"-"+(tripTime%1000000)+"-points.csv"));
		bw2.write("lng,lat,time,hour,distance,time-diff,speed,in-zone\n");
		bw2.write(original+"\n");
		Coordinate lastCoor = null;
		Log lastLog = null; 
		for(int i=0;i<coorList.size();i++){
			Coordinate c = coorList.get(i);
			Log l = list.get(i);
			if(lastCoor == null|| lastLog == null){
				bw2.write(c.x+","+c.y+","+l.getTime()+","+"0"+","+0+","+0+","+0+","+false+"\n");
			}else{
				int distance = (int)(c.distance(lastCoor)*1000*100);//in meter
				int timeDiff = (int)(l.getTime()-lastLog.getTime());
				int speed = -1;
				if(timeDiff!=0){
					speed = (int)(distance/ timeDiff *3.6);//in km/h
				}
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(l.getTime()*1000);
				int hour = cal.get(Calendar.HOUR_OF_DAY);
				bw2.write(c.x+","+c.y+","+l.getTime()+","+hour+","+distance+","+timeDiff+","+speed+","+l.isInQueue()+"\n");
			}
			lastCoor = c;
			lastLog =l;
		}
		bw2.close();
	}

	
	private void loadTrips() throws IOException{
		BufferedReader br;
		long countLine = 0;
		br = new BufferedReader(new FileReader(inputTripFile));
		System.out.println("Loading trips file "+inputTripFile);
		br.readLine();//trim header
		String line = br.readLine();
		while(line!=null){
			try{
				countLine ++;
				String[] tokens = line.split(",", 4);
				int vid = Integer.parseInt(tokens[0]);
				long startTime = Long.parseLong(tokens[2]);
				traceList.add(new Trace(vid,startTime,line));
				if(countLine>30){
					break;
				}
			}catch(NumberFormatException nume){
				nume.printStackTrace();
			}
			line = br.readLine();
		}
		br.close(); 
		System.out.format("Trips loaded. NumOfLine read %s %n", countLine);
	}
	class Trace{
		int vid;
		long startTime;
		String original;
		List<Log> logRecords = new ArrayList<Log>();
		public Trace(int vid, long startTime, String original){
			this.vid = vid;
			this.startTime = startTime;
			this.original = original;
		}

	}

}
