package airport.shorttime;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ExtractShortTime {
	List<String> outputs = new ArrayList<String>();
	public static void main(String[] args) {
		if(args.length==2){
			boolean isMonthly = false;
			for(String arg:args){
				if(arg.indexOf("yy")!=-1 && arg.indexOf("mm")!=-1){
					isMonthly = true;break;
				}
			}
			if(isMonthly){
				String[] years = {"09","10","11","12"};
				String[] months = {"01","02","03","04","05","06","07","08","09","10","11","12"};
				for(int i=0;i<years.length;i++){
					String year = years[i];
					for(int j=0;j<months.length;j++){
						String month = months[j];
						String[] paths = new String[2];
						paths[0] = args[0];
						paths[1] = args[1];
						for(int k=0;k<paths.length;k++){
							paths[k] = paths[k].replaceAll("yy", year);
							paths[k] = paths[k].replaceAll("mm", month);
						}
						new ExtractShortTime(paths[0], paths[1]);
						
					}
				}
			}else{
				new ExtractShortTime(args[0], args[1]);
			}
			
		}else{
			System.out.println("Please provide following arguments. Use yy and mm to replace 2-digit year and month in the path.");
			System.out.println("<path to queue time file> <output path>");
		}
		System.out.println("\n****FINISH****");
	}
	public ExtractShortTime(String inputPath, String outputPath){
		if(read(inputPath)){
			printOutputs(outputPath);
		}
	}
	private boolean printOutputs(String outputPath){
		BufferedWriter br= null;
		boolean success = true;
		long countLine = 0;
		long startTime = System.currentTimeMillis();
		try{
			br = new BufferedWriter(new FileWriter(outputPath));
			br.write("vehicle-id,trip-id,start-time,start-long,start-lat,fare,driver-id,join-queue-time,queue-time, line-num\n");
			for(String line: outputs){
				br.write(line+"\n");
				countLine++;
			}
			br.flush();
			br.close();
		}catch(IOException e){
			e.printStackTrace();
			if(br!=null)
				try {
					br.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			success = false;
		}
		long timeElapse = System.currentTimeMillis()-startTime;
		System.out.format("NumOfLine written %s | Elapse %s%n", countLine,timeElapse);
		return success;
	}
	private boolean read(String inputPath){
		BufferedReader br = null;
		boolean success = true;
		long countLine = 0;
		long startTime = System.currentTimeMillis();
		try{
			br = new BufferedReader(new FileReader(inputPath));
			System.out.println("loading file "+inputPath);
			br.readLine();//trim header
			Random r = new Random();
			/*initial values*/
			String line = br.readLine();
			String[] lineBreak = line.split(",");
			int rDuration =Integer.parseInt(lineBreak[40]);//queuing time in seconds
			while(line!=null){
				countLine++;
				//filter out records with short queuing time
				if(rDuration<2*60){
					//do sampling 
					double rNum = r.nextDouble();
					if(rNum>0.45&&rNum<=0.5){//a random 5% from the filtered records
						outputs.add(line+","+countLine);
					}
				}
				line = br.readLine();
				if(line!=null){
					lineBreak = line.split(",");
					rDuration =Integer.parseInt(lineBreak[40]);
				}
			}
			br.close();
		}catch(IOException e){
			e.printStackTrace();
			if(br!=null)
				//close the reader properly
				try {
					br.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			success = false;
		}
		long timeElapse = System.currentTimeMillis()-startTime;
		System.out.format("records loaded. NumOfLine read %s | Elapse %s%n", countLine,timeElapse);
		return success;

	}

}
