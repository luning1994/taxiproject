package airport.shorttime;

class Log{
	private boolean inQueue;
	private int vid;
	private long time;
	private double lat;
	private double lng;
	Log(int vid, long time, boolean inQueue, double lat, double lng) {
		this.vid = vid;
		this.time = time;
		this.inQueue = inQueue;
		this.lat = lat;
		this.lng = lng;
	}
	boolean isInQueue(){
		return inQueue;
	}
	int getVid(){
		return vid;
	}
	long getTime(){
		return time;
	}
	double getLat(){
		return lat;
	}
	double getLng(){
		return lng;
	}
}

