package airport.special;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TripDivider {
	public static void main(String[] args){
		boolean validInput = false;
		if(args.length>2){
			validInput = true;
			boolean isMonthly = false;
			for(String arg:args){
				if(arg.indexOf("yy")!=-1 && arg.indexOf("mm")!=-1){
					isMonthly = true;break;
				}
			}
			/*Put the fares in an string array*/
			String[] fares = new String[args.length-2];
			for(int i=2;i<args.length;i++){
				fares[i-2] = args[i];
			}
			if(isMonthly){
				String[] years = {"09","10","11","12"};
				String[] months = {"01","02","03","04","05","06",
										"07","08","09","10","11","12"};
				for(int i=0;i<years.length;i++){
					String year = years[i];
					for(int j=0;j<months.length;j++){
						String month = months[j];
						String[] paths = new String[args.length];
						for(int k=0;k<args.length;k++){
							paths[k] = args[k].replaceAll("yy", year)
												.replaceAll("mm", month);
						}
						try{
							devideByFare(paths[0], paths[1], fares);
						}catch(IOException exc){
							System.out.println("File not exist or unaccessible");
							exc.printStackTrace();
						}
					}
				}
			}else{//if not monthly
				/*if not monthly, take the inputs literally*/
				try{
					devideByFare(args[0],args[1], fares);
				}catch(IOException exc){
					System.out.println("File not exist or unaccessible");
					exc.printStackTrace();
				}
			}//end monthly condition
		}
		if(!validInput){
			System.out.println("Please provide following arguments. Use yy and mm to replace 2-digit year and month in the path.");
			System.out.println("<path to special trips file> <output directory> <limo rate1 (use 5000 for 50 SGD)> <limo rate2> <limo rate3> ...");
		}
		System.out.println("\n****FINISH****");
	}
	public static void devideByFare(String inputFile, String outputDir, String[] limoFare) throws IOException{
		List<String> normalTrips = new ArrayList<String>();
		List<String> limoTrips = new ArrayList<String>();
		
		BufferedReader br = null;
		BufferedWriter bw = null;
		try{
			br = new BufferedReader(new FileReader(inputFile));
			String header = br.readLine();//trim header
			String line = br.readLine();
			while(line!=null){
				if(isInfareList (line.split(",")[10], limoFare)){
					limoTrips.add(line);
				}else{
					normalTrips.add(line);
				}
				line = br.readLine();
			}
			if(!normalTrips.isEmpty()){
				String outputFile = getOutputFile(inputFile, outputDir,"normal");
				System.out.println("Processing "+outputFile+"..." );
				bw = new BufferedWriter(new FileWriter(outputFile));
				bw.write(header+"\n");
				for(String r: normalTrips){
					bw.write(r+"\n");
				}
			}
			if(!limoTrips.isEmpty()){
				String outputFile = getOutputFile(inputFile, outputDir,"limo");
				System.out.println("Processing "+outputFile+"..." );
				bw = new BufferedWriter(new FileWriter(outputFile));
				bw.write(header+"\n");
				for(String r: limoTrips){
					bw.write(r+"\n");
				}
			}
		}finally{
			if(null!=br) br.close();
		}
	}
	private static boolean isInfareList(String testFare, String[] limoFare){
		for(String f: limoFare){
			if(testFare.equals(f)) return true;
		}
		return false;
	}
	private static String getOutputFile(String inputFile, String outputDir, String slug){
		
		if(outputDir.substring(outputDir.length()-1)!="/" 
				&&outputDir.substring(outputDir.length()-1)!="\\"){
			outputDir =  outputDir+"/";
		}
		String inputFileName = new File(inputFile).getName();
		String outputFileName = inputFileName.replaceAll(".csv", "-"+slug+".csv");
		return outputDir+outputFileName;
	}
}
