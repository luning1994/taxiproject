package airport.cost;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;

import airport.util.LocationChecker;

public class CalculateAvgCost {
	
	List<String> outputLines = new ArrayList<String>();
	/*durationMap format: day-hour=>duration (in minutes)*/
	Map<String, Long> durationMap = new HashMap<String,Long>();
	Map<String, Long> airportDurationMap = new HashMap<String,Long>();
	Map<String, Long> fareMap = new HashMap<String,Long>();
	List<String> errors = new ArrayList<String>();
	int lastDayOfMonth = 0;
	private LocationChecker checker = new LocationChecker();
	
	
	private static void execute(String trips,String inputShiftFile, String inputAirportTrips, String output){
		CalculateAvgCost cac = new CalculateAvgCost();
		try {
			//skip the following steps on exception
			cac.loadShiftFile(inputShiftFile);
			cac.getAirportDurations(inputAirportTrips);
			cac.loadTrips(trips);
			cac.prepareOutput();
			cac.printout(output);
			//cac.printErrors();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Writer method
	 * @param output
	 * @throws IOException
	 */
	private void printout(String output)throws IOException{
		BufferedWriter br= null;
		try{
			br = new BufferedWriter(new FileWriter(output));
			System.out.println("Printing to " + output);
			br.write("day,hour,total-fare,total-duration,airport-duration,duration,average-fare-duration\n");
			for(String line: outputLines){
				br.write(line+"\n");
			}
			br.flush();
			br.close();
		}catch(IOException e){
			if(br!=null) br.close();
			throw new IOException();
		}
	}
	/**
	 * Prints out errors in the working directory. Format <day><hour><fileName>
	 * @throws IOException
	 */
	private void printErrors() throws IOException{
		try{
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("cost-errors.csv", true)));
			for(String line: errors){
				out.println(line);
			}
			out.flush();
			out.close();
		}catch(IOException e){
			throw new IOException();
		}
	}
	/**
	 * Loads shift file into map
	 * @param inputShiftFile
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void loadShiftFile(String inputShiftFile) throws FileNotFoundException, IOException{
		long timerStart = System.currentTimeMillis();
		long countLine =0;
		BufferedReader sbr;
		//read input shift file. Unzip first if the file extension is gz
		if(inputShiftFile.substring(inputShiftFile.length()-2).equalsIgnoreCase("gz")){
			sbr = new BufferedReader(new InputStreamReader(
						new GZIPInputStream(new FileInputStream(inputShiftFile))));
		}else{
			sbr = new BufferedReader(new FileReader(inputShiftFile));
		}
		//trim off the first 4 lines
		for(int i=1;i<=4;i++){
			sbr.readLine();
		}
		String line = sbr.readLine();
		while(line!=null){
			countLine++;
			/* *********HEADER***********
			 * day,hour,vehicle-id,driver-id,time-in-minutes
			 * **************************
			 */
			String[] rcd = line.split(",");
			String dayHour = rcd[0]+","+rcd[1];
			int duration = Integer.parseInt(rcd[4]);
			//put the shift record in map to accumulate duration
			if(!durationMap.containsKey(dayHour)){
				durationMap.put(dayHour, (long)duration);
			}else{
				durationMap.put(dayHour, durationMap.get(dayHour)+duration);
			}
			line = sbr.readLine();
		}
		sbr.close();
		long timeElapse = System.currentTimeMillis()-timerStart;
		System.out.format("! Hourly total duration has been calculated. \nNumOfLine read %s | Elapse %s%n", countLine,timeElapse);
	}
	/**
	 * Airport duration is defined as the period from join queue time to trip end time
	 * @param inputAirportTrips
	 * @throws IOException
	 */
	private void getAirportDurations(String inputAirportTrips) throws IOException{
		BufferedReader br = null;
		long countLine = 0;
		
		try{
			TimeZone tz = TimeZone.getTimeZone("GMT+8:00");
			Calendar cs = Calendar.getInstance(tz);/*cal for start time*/
			Calendar ce = Calendar.getInstance(tz);/*cal for end time*/
			//read input trip file
			System.out.println("Loading airport trips file for airport duration "+inputAirportTrips);
			br = new BufferedReader(new FileReader(inputAirportTrips));

			String header = br.readLine();//trim header
			List<String> hList = Arrays.asList(header.split(","));
			String line = br.readLine();
			countLine ++;
			while(line!=null){
				try{
					countLine ++;
					String[] rcd = line.split(",");
					long startTime = Long.parseLong(rcd[hList.indexOf("join-queue-time")]);
					long endTime = Long.parseLong(rcd[hList.indexOf("end-time")]);//CHANGED on 23/12/15
					/*interpret times*/
					cs.setTimeInMillis(startTime*1000);
					ce.setTimeInMillis(endTime*1000);
					int startHour 	= cs.get(Calendar.HOUR_OF_DAY);
					int endHour 	= ce.get(Calendar.HOUR_OF_DAY);
					int startDay 	= cs.get(Calendar.DATE);
					int endDay 		= ce.get(Calendar.DATE);
					int startMinute = cs.get(Calendar.MINUTE);
					int endMinute 	= ce.get(Calendar.MINUTE);
					if(startHour==endHour){
						subtractFromDurationMap(startDay, startHour, endMinute-startMinute);
					}else{
						/*in case of trips on the last day of the month overflow to next month*/
						subtractFromDurationMap(startDay, startHour, (long)60-startMinute);
						subtractFromDurationMap(startDay, endHour, (long)endMinute);						
						/*process hours the trip span across, except the head and tail*/
						if(startDay<=endDay){/*for normal cases, just enumerate from the start day start hour to end day end hour*/
							int cDay = startDay;
							int cHour = startHour+1;
							while(cDay<=endDay && cHour<endHour){
								subtractFromDurationMap(cDay, cHour, 60);
								if(cHour<23) {cHour++;}
								//continues to the next day
								else {cHour=0;cDay++;}
							}
						}else{
							/*in case of trips on the last day of the month overflow to next month. 
							 * Assume the end day is the first day of the following month*/
							for(int i=startHour+1;i<=23;i++){
								subtractFromDurationMap(startDay, i, 60);
							}
						}
					}
					
				}catch(NumberFormatException nume){
					nume.printStackTrace();
				}
				line = br.readLine();
			}
			br.close();
			System.out.format("! Airport duration subtracted from duration map| NumOfLines read: %s %n", countLine);
		}catch(IOException e){
			if(br!=null)br.close(); 
			throw new IOException();
		}
		
	}
	/**
	 * Load original trips to calculate accumulated fare per hour. 
	 * The duration is taken as the the join queue time.
	 * @param inputTripFile
	 * @throws IOException
	 */
	private void loadTrips(String inputTripFile) throws IOException{
		BufferedReader br = null;
		long countLine = 0;
		long startTime = System.currentTimeMillis();
		try{
			//read input trip file
			System.out.println("! Loading original trips for gross fare "+inputTripFile);
			br = new BufferedReader(new FileReader(inputTripFile));

			br.readLine();//trim header
			String line = br.readLine();
			countLine ++;
			int lastDay =0;
			/* ****************HEADER:**********************
			 * trip-id,job-id,start-time,end-time,start-long,start-lat,
			 * end-long,end-lat,vehicle-id,distance,fare,duration,start-dow,
			 * start-day,start-hour,start-minute,end-dow,end-day,end-hour,end-minute
			 */
			while(line!=null){
				try{
					countLine ++;
					String[] rcd = line.split(",");
					/*get fare*/
					int fare = Integer.parseInt(rcd[10]);
					/*Get trip start time and end time*/
					
					int startDay = Integer.parseInt(rcd[13]);
					int startHour = Integer.parseInt(rcd[14]);
					int startMinute = Integer.parseInt(rcd[15]);
					int endDay = Integer.parseInt(rcd[17]);
					int endHour = Integer.parseInt(rcd[18]);
					int endMinute = Integer.parseInt(rcd[19]);
					/*Get trip start coordinates*/
					double lat = Double.parseDouble(rcd[5]);
					double lng = Double.parseDouble(rcd[4]);
					/*only count non-airport trips*/
					boolean isAirport = checker.isInAnyQueue(lat, lng);
					/*map to hours*/
					if(!isAirport){
						 mapFareToHours(fare, startDay, startHour, startMinute, endDay, endHour, endMinute);
					}
					lastDay = startDay;
				}catch(NumberFormatException nume){
					nume.printStackTrace();
				}
				line = br.readLine();
			}
			br.close();
			lastDayOfMonth = lastDay;
		}catch(IOException e){
			if(br!=null)br.close(); 
			throw new IOException();
		}
		
		long timeElapse = System.currentTimeMillis()-startTime;
		System.out.format("! Gross fare calculated. NumOfLine read %s | Elapse %s%n", countLine,timeElapse);
	}
	private void prepareOutput(){
		for(int i=1;i<=lastDayOfMonth;i++){
			for(int j=0;j<=23;j++){
				String key = i+","+j;
				boolean validFare = false;
				boolean validDuration = false;
				long fare =0, totalDuration=0, airportDuration=0, duration=0,avgFareDuration = 0;
				if(fareMap.containsKey(key)) {
					validFare=true;
					fare = fareMap.get(key);
				}
				if(durationMap.containsKey(key) && durationMap.get(key)!=0) {
					if(airportDurationMap.containsKey(key)) {
						airportDuration = airportDurationMap.get(key)/60;//in hours
					}
					validDuration = true;
					totalDuration = durationMap.get(key)/60;//in hours
					duration = totalDuration - airportDuration;
					if(duration< 0 ) System.out.println("ERR: negative duration "+totalDuration+","+airportDuration);
					avgFareDuration = fare / duration;
				}
				if(validFare && validDuration){
					outputLines.add(key+","+fare+","+totalDuration+","+airportDuration+","+duration+","+avgFareDuration);
				}else if(validFare){//missing duration
					outputLines.add(key+","+fare+",ERR,ERR,ERR,ERR");
				}else{//missing fare
					outputLines.add(key+",ERR,"+totalDuration+","+airportDuration+","+duration+",ERR");
				}
			}
		}
		System.out.println("Ready to print out ");
	}
	/**
	 * Used when 
	 * @param day
	 * @param hour
	 * @param fare
	 */
	private void addToFareMap(int day, int hour, long fare){
		String key = day+","+hour;
		if(fareMap.containsKey(key)){
			fareMap.put(key, fareMap.get(key)+fare);
		}else{
			fareMap.put(key,fare);
		}
	}
	private void subtractFromDurationMap(int day, int hour, long duration){//duration in minutes
		String key = day+","+hour;
		if(airportDurationMap.containsKey(key)){
			airportDurationMap.put(key, airportDurationMap.get(key)+duration);
		}else{
			airportDurationMap.put(key,duration);
		}
	}
	private void mapFareToHours(int fare, int startDay, int startHour, int startMinute, int endDay, int endHour, int endMinute){
		if(startHour==endHour){
			addToFareMap(startDay, startHour, (long) fare);
		}else{
			double perMinuteFare = fare/((endDay - startDay)*60*24 + (endHour-startHour)*60-startMinute+endMinute);
			/*in case of trips on the last day of the month overflow to next month. 
			 * I have to assume the end day is the first day of the following month*/
			if(endDay-startDay>=-30 && endDay-startDay<-20){
				perMinuteFare = fare/(60*24 + (endHour-startHour)*60-startMinute+endMinute);
			}
			/*process head and tail*/
			addToFareMap(startDay, startHour, (long) perMinuteFare*(60-startMinute));
			addToFareMap(startDay, endHour, (long) perMinuteFare*endMinute);
			/*process hours the trip span across, except the head and tail*/
			if(startDay<=endDay){/*for normal cases, just enumerate from the start day start hour to end day end hour*/
				int cDay = startDay;
				int cHour = startHour+1;
				while(cDay<=endDay && cHour<endHour){
					addToFareMap(cDay, cHour, (long) perMinuteFare*60);
					if(cHour<23) {cHour++;}
					//continues to the next day
					else {cHour=0;cDay++;}
				}
			}else{
				/*in case of trips on the last day of the month overflow to next month. 
				 * Assume the end day is the first day of the following month*/
				for(int i=startHour+1;i<=23;i++){
					addToFareMap(startDay, i, (long) perMinuteFare*60);
				}
			}
		}
	}
	public static void main(String[] args) {
		if(args.length==4){
			boolean isMonthly = false;
			for(String arg:args){
				if(arg.indexOf("yy")!=-1 && arg.indexOf("mm")!=-1){
					isMonthly = true;
					break;
				}
			}
			if(isMonthly){
				String[] years = {"09","10","11","12"};
				String[] months = {"01","02","03","04","05","06","07","08","09","10","11","12"};
				for(int i=0;i<years.length;i++){
					String year = years[i];
					for(int j=0;j<months.length;j++){
						String month = months[j];
						String[] paths = new String[]{args[0],args[1],args[2],args[3]};
						for(int k=0;k<paths.length;k++){
							paths[k] = paths[k].replaceAll("yy", year);
							paths[k] = paths[k].replaceAll("mm", month);
						}
						execute(paths[0], paths[1], paths[2],paths[3]);
					}
				}
			}else{
				//execute with paths for a single set
				execute(args[0], args[1], args[2],args[3]);
			}
		}else{
			System.out.println("Please provide following arguments. Use yy and mm to replace 2-digit year and month in the path.");
			System.out.println(" <path to original trips file> <path to shift file> <path to airport trip file> <output file>");
		}
		System.out.println("\n****FINISH****");
	}
		

}
