package airport.cost;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateCost {
	String fileHeader = null;
	Map<String, Fare> fareMap = new HashMap<String, Fare>();
	List<String> outputs = new ArrayList<String>();
	public static void main(String[] args) {
		if(args.length==3){
			boolean isMonthly = false;
			for(String arg:args){
				if(arg.indexOf("yy")!=-1 && arg.indexOf("mm")!=-1){
					isMonthly = true;break;
				}
			}
			if(isMonthly){
				String[] years = {"09","10","11","12"};
				String[] months = {"01","02","03","04","05","06","07","08","09","10","11","12"};
				for(int i=0;i<years.length;i++){
					String year = years[i];
					for(int j=0;j<months.length;j++){
						String month = months[j];
						String[] paths = new String[]{args[0],args[1],args[2]};
						for(int k=0;k<paths.length;k++){
							paths[k] = paths[k].replaceAll("yy", year);
							paths[k] = paths[k].replaceAll("mm", month);
						}
						new CalculateCost(paths[0], paths[1], paths[2]);
					}
				}
			}else{
				//execute with paths for a single set
				new CalculateCost(args[0], args[1], args[2]);
			}
			
		}else{
			System.out.println("Please provide following arguments. Use yy and mm to replace 2-digit year and month in the path.");
			System.out.println(" <path to queue time file> <path to avg cost file> <output path>");
		}

		
		System.out.println("\n****FINISH****");
	}
	public CalculateCost(String inputPath, String costFilePath, String outputPath){
		if(loadAvgCostFile(costFilePath)){
			if(appendCost(inputPath)){
				printOutputs(outputPath);
			}
		}
	}
	/**
	 * Writer method
	 * @param outputPath
	 * @return
	 */
	private boolean printOutputs(String outputPath){
		BufferedWriter br= null;
		boolean success = true;
		long countLine = 0;
		long startTime = System.currentTimeMillis();
		try{
			br = new BufferedWriter(new FileWriter(outputPath));
			//write header
			br.write(fileHeader + ",opportunity-cost-for-queuing,"
					+ "overall-opportunity-cost,economic-profit\n");
			for(String line: outputs){
				br.write(line+"\n");
				countLine++;
			}
			br.flush();
			br.close();
		}catch(IOException e){
			e.printStackTrace();
			if(br!=null)
				try {
					br.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			success = false;
		}
		long timeElapse = System.currentTimeMillis()-startTime;
		System.out.format("File %s|NumOfLine written %s | Elapse %s%n",outputPath, countLine,timeElapse);
		return success;
	}
	/**
	 * Calculation method
	 * @param inputPath the queue time input path
	 * @return
	 */
	private boolean appendCost(String inputPath){
		BufferedReader br = null;
		boolean success = true;
		long countLine = 0;
		long startTime = System.currentTimeMillis();
		try{
			br = new BufferedReader(new FileReader(inputPath));
			System.out.println("loading file "+inputPath);
			String header = br.readLine();//trim header
			fileHeader = header;
			List<String> hList = Arrays.asList(header.split(","));
			/*initialize values*/
			String line = br.readLine();
			while(line!=null){
				String[] lineBreak = line.split(",");
				//extract attributes
				int startDay = Integer.parseInt(lineBreak[hList.indexOf("start-day")]);
				int startHour = Integer.parseInt(lineBreak[hList.indexOf("start-hour")]);
				int queueTime = Integer.parseInt(lineBreak[hList.indexOf("queue-time")]);//in seconds
				int tripTime = Integer.parseInt(lineBreak[hList.indexOf("end-time")])
											- Integer.parseInt(lineBreak[hList.indexOf("start-time")]);//inseconds
				int tripProfit = Integer.parseInt(lineBreak[hList.indexOf("fare")]);
				
				String key = startDay+","+startHour;
				//look up the fareMap for the day-hour
				if(fareMap.containsKey(key)){
					Fare f = fareMap.get(key);
					if(f.getFare()==-1){//invalid fare
						String outputLine = line+",-1"+",-1"+",-1";//produce place holders
						outputs.add(outputLine);
					}else{//valid
						int queueCost = (int)( queueTime / 3600.0 *f.getFare());//fare for the queuing duration
						int tripCost = (int)(tripTime / 3600.0 *f.getFare());//fare for the trip duration
						int overallCost = queueCost + tripCost;
						int econProfit = tripProfit - overallCost;
						String outputLine = line+","+queueCost+","+overallCost+","+econProfit;
						outputs.add(outputLine);
					}
				}else{
					System.out.println("Entry missing for "+key);
				}
				countLine++;
				line = br.readLine();
			}
			br.close();
		}catch(IOException e){
			e.printStackTrace();
			//make sure br is closed
			if(br!=null)
				try {
					br.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			success = false;
		}
		long timeElapse = System.currentTimeMillis()-startTime;
		System.out.format("records loaded. NumOfLine read %s | Elapse %s%n", countLine,timeElapse);
		return success;
	}
	/**
	 * Load avgCostFile into map
	 * @param costFilePath
	 * @return ifSuccess
	 */
	private boolean loadAvgCostFile(String costFilePath){
		BufferedReader br = null;
		boolean success = true;
		long countLine = 0;
		long startTime = System.currentTimeMillis();
		try{
			br = new BufferedReader(new FileReader(costFilePath));
			System.out.println("Loading cost file "+costFilePath);
			br.readLine();//trim header
			String line = br.readLine();
			while(line!=null){
				try{
					countLine ++;
					/* ***************HEADER**********************
					 * day,hour,fare,total-duration,airport-duration,duration,average-fare-duration
					 */
					String[] rcd = line.split(",");
					for(int i=0;i<rcd.length;i++){
						if(rcd[i].equals("ERR")) rcd[i] = "-1";
					}
					int day = Integer.parseInt(rcd[0]);
					int hour = Integer.parseInt(rcd[1]);
					int fare = Integer.parseInt(rcd[6]);
					//constructs new fare object and put in map
					Fare f = new Fare(day,hour,fare);
					fareMap.put(day+","+hour,f);
				}catch(NumberFormatException nume){
					nume.printStackTrace();
				}
				line = br.readLine();
			}
			br.close();
			long timeElapse = System.currentTimeMillis()-startTime;
			System.out.format("file loaded. NumOfLine read %s | Elapse %s%n", countLine,timeElapse);
		}catch(IOException e){
			e.printStackTrace();
			if(br!=null)
				try {
					br.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			success = false;
			
		}
		return success;
	}
}
