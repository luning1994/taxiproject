package airport.duration;

class Trip{
	private String tripRecord;
	private int vid;
	private long time;
	
	public Trip(int vehicleId, long startTime, String tripRecord){
		this.vid = vehicleId;
		this.time = startTime;
		this.tripRecord = tripRecord;
	}
	
	public int getVid(){
		return vid;
	}
	public long getTime(){
		return time;
	}
	public String toString(){
		return tripRecord;
	}
}
