package airport.duration;

class Log{
	private boolean inQueue;
	private int vid;
	private long time;
	public Log(int vid, long time, boolean inQueue) {
		this.vid = vid;
		this.time = time;
		this.inQueue = inQueue;
	}
	public boolean isInQueue(){
		return inQueue;
	}
	public int getVid(){
		return vid;
	}
	public long getTime(){
		return time;
	}
}
