package airport.duration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.LinkedBlockingQueue;

import airport.util.LocationChecker;

/**
 * @author Alex
 *
 */
public class CalculateDuration implements Runnable{

	final long MAX_RECORD_LIFE = 30*60;//The threshold the car record data are recycled
	final long MAX_ENTRY_LIFE = 24*60*60;//The threshold the entry record data are recycled
	
	private List<Trip> tripList = new ArrayList<Trip>();
	private String inputLogFile;
	private String inputTripFile;
	private LocationChecker checker = new LocationChecker();
	private LinkedBlockingQueue<String> outputQue;
	private LinkedBlockingQueue<String> errorQue;
	private static HashMap<Integer,Log> timeMap = new HashMap<Integer,Log>();
	private long timeOfRecord = 0;
	
	
	public CalculateDuration(String inputLogFile,String inputTripFile,
							LinkedBlockingQueue<String> outputQue,LinkedBlockingQueue<String> errorQue){
		this.inputLogFile = inputLogFile;
		this.inputTripFile = inputTripFile;
		this.outputQue = outputQue;
		this.errorQue = errorQue;
	}
	public static void execute(String inputLogFile, String inputTripFile, String outputFile, String errorFile){
		File[] files = new File[3];
		files[0] = new File(inputLogFile);
		files[1] = new File(inputTripFile);
		files[2] = new File(outputFile);
		System.out.println("Targeting files:");
		for(File f:files){
			System.out.println(f.getAbsolutePath());
		}
		LinkedBlockingQueue<String> outputQue = new LinkedBlockingQueue<String>();
		LinkedBlockingQueue<String> errorQue = new LinkedBlockingQueue<String>();
		//construct objects
		CalculateDuration cd = new CalculateDuration(inputLogFile, inputTripFile, outputQue, errorQue);
		WriterThread wt1 = new WriterThread(outputFile,outputQue);
		WriterThread wt2 = new WriterThread(errorFile,errorQue);
		//creates threads
		Thread wt1Thread = new Thread(wt1);
		Thread wt2Thread = new Thread(wt2);
		Thread cdThread  = new Thread(cd);
		//kick them off
		wt1Thread.start();
		wt2Thread.start();
		cdThread.start();
		//wait for all threads to finish
		try {
			wt1Thread.join();
			wt2Thread.join();
			cdThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void run(){
		//counters and timers
		long countLine = 0;
		long countAdded = 0;
		long startTime = System.currentTimeMillis();
		/*load trips, if file not found, put an end to terminate writer*/
		try {
			loadTrips();
		} catch (IOException e1) {
			try {
				outputQue.put("end");
				errorQue.put("end");
			} catch (InterruptedException e) {
			}
			e1.printStackTrace();
		}
		
		BufferedReader br;
		Iterator<Trip> tripIter = tripList.iterator();
		
		try {
			br = new BufferedReader(new FileReader(inputLogFile));
			System.out.println("loading file "+inputLogFile);
			br.readLine();
			//peek next record
			String line = br.readLine();
			Trip trip = tripIter.next();
			Log log = parseLog(line);
			
			while(log!=null && tripIter.hasNext()){
				countLine ++;
				if(log.getTime() < trip.getTime()){
					int vid = log.getVid();
					timeOfRecord = log.getTime();
					if(!timeMap.containsKey(vid) && !log.isInQueue()){
						timeMap.put(vid, log);
					}else if(timeMap.containsKey(vid)){
						Log lastRecord = timeMap.get(vid);//get last one
						Log lastLog = lastRecord;
						/*if last log is not in queue, update; else if this 
						 * not in queue means the taxi left airport without trip*/
						if(!lastLog.isInQueue() || !log.isInQueue()){
							timeMap.put(vid, log);//replace last record with the the new log
						}
						/*if last log in queue, this log also in queue, do nothing*/
					}
					/*get a new log*/
					log = parseLog(br.readLine());
				}else{ //it's trip's turn
					int vid = trip.getVid();
					timeOfRecord = trip.getTime();
					Log lastLog;
					if(timeMap.containsKey(vid) && (lastLog=timeMap.get(vid)).isInQueue()){
						countAdded++;
						processMatchFound(lastLog, trip);
						timeMap.remove(vid);
					}else{ //no legitimate lastLog found
						processDiscard(trip);
					}
					trip = tripIter.next();
				}
				printProgress(countLine);
			}
			br.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		/*terminate writer*/
		try {
			outputQue.put("end");
			errorQue.put("end");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		long timeElapse = System.currentTimeMillis()-startTime;
		System.out.format("NumOfLine read %s  | NumbOfLine added %s | Elapse %s%n", countLine, countAdded,timeElapse);
	}
	private void printProgress(long countLine){
		/*Progress printer*/
		if(countLine % 10000000==0){
			if(countLine % 100000000==0){
				int carCount = purgeOldRecords();
				System.out.println((countLine/1000000) + " million lines read | records removed: "+carCount);
			}else{
				System.out.println((countLine/1000000) + " million lines read");
			}
		}
		/*Progress printer ends*/
	}
	private int purgeOldRecords(){
		int count =0;
		long oldRecordThreshold = timeOfRecord - MAX_RECORD_LIFE;
		long oldEntryThreshold = timeOfRecord - MAX_ENTRY_LIFE;
		Iterator<Entry<Integer, Log>> entries = timeMap.entrySet().iterator();
		while(entries.hasNext()){
			Entry<Integer, Log> entry = (Entry<Integer, Log>) entries.next();
			Log value = entry.getValue();
			if((value.isInQueue() && value.getTime()<oldEntryThreshold)
					||(!value.isInQueue() && value.getTime()<oldRecordThreshold)){
				entries.remove();
				count++;
			}
        }
		return count;
	}
	private Log parseLog(String inputLine){
		if(inputLine==null){
			return null;
		}
		String[] rcd = inputLine.split(",");
		long time = Long.parseLong(rcd[0]);
		int vid =Integer.parseInt(rcd[1]);
		double lng = Double.parseDouble(rcd[3]);
		double lat = Double.parseDouble(rcd[4]);
		return new Log(vid, time, checker.isInOuterQueue(lat, lng));
	}
	private void processMatchFound(Log lastLog, Trip trip) throws InterruptedException{
		long lastLogTime = lastLog.getTime();
		long duration = trip.getTime() - lastLogTime;
		String toPut = trip.toString()+","+lastLogTime+","+duration;
		//put to output queue
		outputQue.put(toPut);
		
	}
	private void processDiscard(Trip trip) throws InterruptedException{
		errorQue.put(trip.toString());	
	}

	
	private void loadTrips() throws IOException{
		BufferedReader br;
		long countLine = 0;
		long startTime = System.currentTimeMillis();
		br = new BufferedReader(new FileReader(inputTripFile));
		System.out.println("Loading trips file "+inputTripFile);
		String header = br.readLine();//trim header
		try {
			outputQue.put(header+",join-queue-time,queue-time");
			errorQue.put(header);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String line = br.readLine();
		while(line!=null){
			try{
				countLine ++;
				String[] rcd = line.split(",",5);
				long time = Long.parseLong(rcd[3]);
				int vid = Integer.parseInt(rcd[0]);
				Trip trip = new Trip(vid,time,line);
				tripList.add(trip);
			}catch(NumberFormatException nume){
				nume.printStackTrace();
			}
			line = br.readLine();
		}
		br.close(); 
		long timeElapse = System.currentTimeMillis()-startTime;
		System.out.format("Trips loaded. NumOfLine read %s | Elapse %s%n", countLine,timeElapse);
	}
	
	public static void main(String[] args) {
		if(args.length==4){
			boolean isMonthly = false;
			for(String arg:args){
				if(arg.indexOf("yy")!=-1 && arg.indexOf("mm")!=-1){
					isMonthly = true;break;
				}
			}
			if(isMonthly){
				String[] years = {"09","10","11","12"};
				String[] months = {"01","02","03","04","05","06","07","08","09","10","11","12"};
				for(int i=0;i<years.length;i++){
					String year = years[i];
					for(int j=0;j<months.length;j++){
						String month = months[j];
						String[] paths = {args[0],args[1],args[2],args[3]};
						for(int k=0;k<paths.length;k++){
							paths[k] = paths[k].replaceAll("yy", year);
							paths[k] = paths[k].replaceAll("mm", month);
						}
						execute(paths[0], paths[1], paths[2], paths[3]);
					}
				}
			}else{
				execute(args[0],args[1], args[2], args[3]);
			}
		}else{
			System.out.println("Please provide following arguments. Use yy and mm to replace 2-digit year and month in the path.");
			System.out.println("<path to logs file> <path to trips file> <output file path> <output-error file path>");
		}
		System.out.println("\n****FINISH****");
	}
	
}
