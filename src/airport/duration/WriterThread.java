package airport.duration;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

public class WriterThread implements Runnable{
	private String outputFile;
	private LinkedBlockingQueue<String> bufQue;
	
	public WriterThread(String outputFile,LinkedBlockingQueue<String> bufQue){
		this.outputFile = outputFile;
		this.bufQue = bufQue;
	}
	
	public void run(){
		long numWritten = -1;//minus away header line
		BufferedWriter bw = null; 
		try {
			FileWriter fw = new FileWriter(outputFile);
			bw = new BufferedWriter(fw);
			String line;
			while(!(line=bufQue.take()).equals("end")){
				bw.write(line+"\n");
				numWritten++;
				//System.out.println(line);
			}
			bw.flush();
			bw.close();
			System.out.println(outputFile+ " numOfLines "+numWritten);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("interruptedException");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
