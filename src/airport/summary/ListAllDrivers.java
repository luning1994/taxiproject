package airport.summary;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class ListAllDrivers {
	private static Set<Long> drivers = new TreeSet<Long>();
	public static void main(String[] args){
		boolean validInput = false;
		if(args.length==2){
			validInput = true;
			boolean isMonthly = false;
			if(args[0].indexOf("yy")!=-1 && args[0].indexOf("mm")!=-1){
				isMonthly = true;
			}
			if(isMonthly){
				String[] years = {"09","10","11","12"};
				String[] months = {"01","02","03","04","05","06",
										"07","08","09","10","11","12"};
				for(int i=0;i<years.length;i++){
					String year = years[i];
					for(int j=0;j<months.length;j++){
						String month = months[j];
						readFile(args[0].replaceAll("yy", year).replaceAll("mm", month));
					}
				}
			}else{//if not monthly
				/*if not monthly, take the inputs literally*/
				readFile(args[0]);
			}//end monthly condition
			writeDrivers(args[1]);
		}
		if(!validInput){
			System.out.println("Please provide following arguments."
					+ " Use yy and mm to replace 2-digit year and month in the path.");
			System.out.println("<path to shiftfiles> <output file> ");
		}
		System.out.println("\n****FINISH****");
	}
	
	public static void readFile(String inputFile){
		List<String> normalTrips = new ArrayList<String>();
		List<String> limoTrips = new ArrayList<String>();
		
		BufferedReader br = null;
		
		try{
			br = new BufferedReader(new FileReader(inputFile));
			br.readLine();//trim header
			br.readLine();//trim header
			br.readLine();//trim header
			br.readLine();//trim header
			String line = br.readLine();
			while(line!=null){
				drivers.add(Long.parseLong(line.split(",")[3]));
				line = br.readLine();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public static void writeDrivers(String outputFile){
		BufferedWriter bw = null;
		try {
			System.out.println("Writing to "+outputFile+"..." );
			bw = new BufferedWriter(new FileWriter(outputFile));
			for(Long r: drivers){
				bw.write(r+"\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
