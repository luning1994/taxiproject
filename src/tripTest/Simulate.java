package tripTest;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import airport.trip.Trip;
import airport.util.LocationChecker;

/**
 * The class continues with the output of GetTrips and tests the map algorithm
 * 
 * trips-0901-test-1
 * 1 - T(airport trip)
 * 2 - T
 * 3 - F(non-airport trip)
 * 4 - F
 * 5 - T
 * 6 - T
 * 7 - F
 * 8 - T
 * 9 - T
 * 0 - F
 * A - T
 * B - T
 * Expected output: <airport trip><prev-trip><next-trip>
 * 2,3,1
 * 5,4,6
 * 6,5,7
 * 8,7,9
 * 9,8,0
 * A,0,B
 * Actual:
 * Same
 * ***********************
 * trips-0901-test-2
 * 1 - T
 * 2 - T
 * 3 - F
 * 4 - F
 * 5 - T
 * 6 - T
 * 7 - T
 * 8 - T
 * Expected output: <airport trip><prev-trip><next-trip>
 * 2,1,3
 * 5,4,6
 * 6,5,7
 * 7,6,8
 * 8,7,9
 * Actual:
 * Same
 * ***********************
 * trips-0901-test-3
 * 1 - F
 * 2 - F
 * 3 - T
 * 4 - T
 * 5 - F
 * 6 - F
 * 7 - T
 * 8 - T
 * Expected output: <airport trip><prev-trip><next-trip>
 * 3,2,4
 * 4,3,5
 * 7,6,8
 * Actual:
 * Same
 */
public class Simulate {
	static LocationChecker checker = new LocationChecker();
	static Map<Integer, Trip[]> tripMap = new HashMap<Integer, Trip[]>(); // vid:tripObj
	static List<Trip[]> outputArray 	 = new ArrayList<Trip[]>();
	static List<Trip[]> lastOutputArray = new ArrayList<Trip[]>();
	public static void main(String[] args){
		//buffered reader for trip files
		//String fileInput = "F:\\RA\\trips-0903-sm.csv";
		String fileInput = "F:\\RA\\test-trips\\trips-0901-test-3.csv";
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(fileInput));
	
	//		//read headers
	//		br.readLine();
	//		exbr.readLine();
			String line = br.readLine();
	
			while(line!=null){
				String records[] = (line).split(",");
				try{
					Trip t = parseLine(records);//throws exception upon reading new header
					mapTrip(t);
					line = br.readLine();
				}catch(NumberFormatException numExc){
					System.out.println("new header detected");
					line = br.readLine();
					if(line!=null){
						String nextRecords[] = (line).split(",");
						Trip t = parseLine(nextRecords);
					}
				}
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(tripMap.get(1553)[0]+"\t\t\t\t\t"+tripMap.get(1553)[1]+"\t\t\t\t\t"+tripMap.get(1553)[2]);
		writeFile(outputArray,"");
	}
	private static Trip parseLine(String[] line) throws NumberFormatException {
		/*
		 * header indices 0 1 2 3 4 5 6
		 * trip-id,job-id,start-time,end-time,start-long,start-lat,end-long, 7 8
		 * 9 10 11 12 13 14 15 16 17
		 * end-lat,vehicle-id,distance,fare,duration,start-dow,start-day,start-
		 * hour,start-minute,end-dow,end-day, 18 19 20 21 22 23 24 end-hour,
		 * end-minute,start-zone,end-zone,driver-id,start-postal,end-postal
		 * 
		 * public Trip(int vehicleId,String tripId, long startTime, double
		 * startLong, double startLat, int fare, long endTime, double endLng,
		 * double endLat, int driverId, double distance, int startDow, int
		 * startDay, int startHour){
		 */
		Trip trip = new Trip(Integer.parseInt(line[8])// int vehicleId
		, line[0]// String tripId
		, Long.parseLong(line[2])// long startTime
		, Double.parseDouble(line[4])// double startLong
		, Double.parseDouble(line[5])// double startLat
		, Integer.parseInt(line[10])// int fare
		, Long.parseLong(line[3])// long endTime
		, Double.parseDouble(line[6])// double endLng
		, Double.parseDouble(line[7])// double endLat
		, Integer.parseInt(line[22])// int driverId
		, Double.parseDouble(line[9])// double distance
		, Integer.parseInt(line[12])// int startDow
		, Integer.parseInt(line[13])// int startDay
		, Integer.parseInt(line[14])// int startHour
		);
		return trip;
	}
	private static void mapTrip(Trip t) {
		int vid = t.getVehicleId();
		String queueName;
		double lat = t.getStartLat();
		double lng = t.getStartLong();
		boolean isInqueue = checker.isInAnyQueue(lat,lng);
		queueName = checker.inQueueName(lat,lng);
		System.out.println(queueName);
		System.out.println(isInqueue);
		
		if (queueName == null) {
			if (!tripMap.containsKey(vid)) {
				tripMap.put(vid, new Trip[] { t, null, null });
			} else if (tripMap.get(vid)[1] == null) {// this triplet is does not
														// contain airport trip
				tripMap.get(vid)[0] = t;
			} else {
				tripMap.get(vid)[2] = t;
				processRemoval(vid, true);
				//added on Tue Oct 20
				tripMap.put(vid, new Trip[] { t, null, null });
			}
		} else { // process airport trip
			t.setStartQueueName(queueName);
			if (!tripMap.containsKey(vid)) {
				/*this triplet will die in the map unless we print it out at the end*/
				tripMap.put(vid, new Trip[] { t, null, null });
			} else if (tripMap.get(vid)[1] == null) {// this triplet is does not
														// contain airport trip
				tripMap.get(vid)[1] = t;
			} else { // when the previous trip is also airport trip,
						// complete the current triplet and initiate a new one
				tripMap.get(vid)[2] = t;
				Trip lastApTrip = tripMap.get(vid)[1];
				processRemoval(vid, true);
				tripMap.put(vid, new Trip[] { lastApTrip, t, null });
			}
		}
	}
	private static void processRemoval(int vid, boolean removeFromMap) {
		Trip[] trip = tripMap.get(vid);
		outputArray.add(trip);
		//for the error loop
		if (removeFromMap) {
			tripMap.remove(vid);
		}
	}

	public static void writeFile(List<Trip[]> arrayToWrite, String fileName) {
//		long numWritten = arrayToWrite.size();
//		long startTime = System.currentTimeMillis();
		System.out.println(arrayToWrite.size());
			for (int i = 0; i < arrayToWrite.size(); i++) {
				//Trip[] trips = arrayToWrite.get(i);
				System.out.println(convertTripletToString(arrayToWrite.get(i)));
				
			}
//
//		long timeElapse = System.currentTimeMillis() - startTime;
//		System.out.format("NumOfLine written %s Elase Time %s %n", numWritten, timeElapse);
	}
	private static String convertTripletToString(Trip[] tripArr) {
		String toWrite = "";
		toWrite += tripArr[1].toString() + ",";
		if (tripArr[0] != null) {
			toWrite += tripArr[0].toString(tripArr[1], false) + ",";
		} else {
			toWrite += ",,,,,,,,,,,,";// use ","*12 as placeholder
		}
		if (tripArr[2] != null) {
			toWrite += tripArr[2].toString(tripArr[1], true) + ",";
		} else {
			toWrite += ",,,,,,,,,,,,";
		}
		return trimComma(toWrite);
	}
	private static String trimComma(String str) {
		return str.substring(0, str.length() - 1);
	}
}
