package tripTest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import airport.util.LocationChecker;

public class GetTrips {
	static LocationChecker checker = new LocationChecker();
	public static void main(String[] args) {
		try {
			String fileInput = "F:\\RA\\trips-0901-sm.csv";
			String outputFile = "F:\\RA\\trips-0901-sm-1024-output.csv";
			String targetVid = "1553";
			BufferedReader br = new BufferedReader(new FileReader(fileInput));
			br.readLine();
			String line = br.readLine();
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(outputFile)));
			
			while(line!=null){
				String records[] = line.split(",");
					if(records[8].equals(targetVid)){
						double lng = Double.parseDouble(records[4]);// double endLng
						double lat =  Double.parseDouble(records[5]);// double endLat
						boolean isAirport =  checker.isInAnyQueue(lat, lng);
						//if(isAirport){
						String inQueue = ",0,-1,-1,-1";
						if(isAirport){
							inQueue = ",1,-1,-1,-1";
						}
						pw.println(line+inQueue);
						System.out.println(line+inQueue);
						//}
					}
					line = br.readLine();
			}
			br.close();
			pw.flush();
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("done");
		
	}

}
