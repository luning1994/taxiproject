# Setup #

### Step 1 -- combine trips and trip ext file to single files for continuous processing
	a. Bash command to get combined trips file and trips-ext file:
		$ cd /home/taxi
		$ arr1=($(find .  -regex './[0-9]*/[0-9]*/trips/trips-[0-9]*-normal.csv -print | sort))
		$ arr2=($(find .  -regex './[0-9]*/[0-9]*/trips/trips-[0-9]*-normal-ext.csv -print | sort))
		$ cat ${arr1[*]} >> <trip file path>
		$ cat ${arr2[*]} >> <trip ext file path>
	b. The headers of original files are kept to differentiate trips from consecutive months
	c. This will take around 10 minutes 
	
### Step 2 -- get airport trip groups from combined trip file 
	a. $ cd <taxi java project root directory>
	   $ java taxi.trip.ProcessTrip <trips file name> <extension file name> <output file directory>
	b. The trip group will be written to <output file directory> The error for each month (except last month) 
will be printed out as well
	c. This will take a few hours
	
### Step 3 --  to get queue time, run
	a.  $ cd <taxi java project root directory>
		$ java taxi.cal2.CalculateDuration <Base Dir> <logs file path from base dir> <trips file path from base dir> <output file path from base dir> <output-error file path from base dir>
	b. Queue time info will be attached at the end of each row.
	c. This will take hours
	
### Step 4 -- get average fare per working hour by hour
	a.  $ cd <taxi java project root directory>
		$ java taxi.cost.CalculateAvgCost <path to original trips file> <path to shift file> <path to airport queue time file> <output path>
	b. Fares only take account of non-airport trips. Duration will be taken as total duration from shifts less (airport queue time + trip time). Fares and duration that span across multiple hours (even days) will be contribute to all these hours by the correct proportion. Granularity is to minutes, even if the times were captured in seconds. 
	c. In case of missing shift or trip record for an hour, ERR will be printed.
	d. This takes around half an hour to execute
	
### Step 5 -- get opportunity cost and economic profit for each airport trip
	a.  $ cd <taxi java project root directory>
		$ java taxi.cost.CalculateAvgCost <path to queue time file> <path to average cost file> <output path>
	b. This will take up to a few second.